$( document ).ready( function () {
    //START TO CREATE MAPA AND SET VALUE DEFAULT
    let map = L.map( 'map' ).setView( [32.47964619410744, -115.30288696289064], 10 )

    L.tileLayer( `https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png`, {
        maxZoom: 18,
    } ).addTo( map );

    //END TO CREATE MAPA AND SET VALUE DEFAULT


    var countUnits = [];

    // START CUSTOM BUTTON ON MAP FOR INTERACTION BAR
    var flag = 1;
    L.easyButton( 'fas fa-bars', function ( btn, map ) {
        if ( flag == 1 ) {
            $( '#sidebar-right' ).addClass( 'active' );
            $( '.overlay-sidebar-right' ).addClass( 'active' );
            $( '.collapse.in' ).toggleClass( 'in' );
            $( 'a[aria-expanded=true]' ).attr( 'aria-expanded', 'false' );
            flag = 0;
        }
        else if ( flag == 0 ) {
            $( '#sidebar-right' ).removeClass( 'active' );
            $( '.overlay-sidebar-right' ).removeClass( 'active' );
            flag = 1;
        }

    } ).addTo( map );
    // END CUSTOM BUTTON ON MAP FOR INTERACTION BAR
    // END SIDEBAR CODE INTERACTION

    //START CUSTOM BUTTON TO CENTER MAP OF YOU
    L.easyButton( 'fas fa-map-pin', function ( btn, map ) {
        navigator.geolocation.getCurrentPosition(
            ( pos ) => {
                const { coords } = pos;
                const { latitude, longitude } = coords;
                map.setView( [latitude, longitude], 10 );
                setTimeout( () => {
                    map.panTo( new L.LatLng( latitude, longitude ) )
                }, 5000 )
            },
            ( error ) => {
                console.log( error )
            },
            {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            } );

    } ).addTo( map );
    //CALCULATE DISTANCE
    var latitudeLastPoint, longitudeLastPoint;
    L.easyButton( 'fas fa-location-arrow', function ( btn, map ) {
        var routeControl = L.Routing.control( {
        } ).addTo( map );
        // routeControl.setWaypoints(route);
        // route=[];
        // routeControl.setWaypoints([]);
        routeControl.
            navigator.geolocation.getCurrentPosition(
                ( pos ) => {
                    const { coords } = pos;
                    const { latitude, longitude } = coords;
                    // map.setView([latitude, longitude], 10);
                    routeControl.setWaypoints( [latitudeLastPoint, longitudeLastPoint] );
                    // L.Routing.control({
                    //     show: true,
                    //     addWaypoints: false,
                    //     draggableWaypoints: false,
                    //     fitSelectedRoutes: false,
                    //     waypoints: [
                    //         L.latLng(latitude, longitude),
                    //         L.latLng(latitudeLastPoint, longitudeLastPoint)
                    //     ]
                    // }).addTo(map);
                    setTimeout( () => {
                        map.panTo( new L.LatLng( latitude, longitude ) )
                    }, 5000 )
                },
                ( error ) => {
                    console.log( error )
                },
                {
                    enableHighAccuracy: true,
                    timeout: 5000,
                    maximumAge: 0
                } );



    } ).addTo( map );
    // END CUSTOM BUTTON TO CENTER MAP OF YOU

    // START CREATING ICONS FOR DISPLAY ON MAP
    var iconMarker = L.icon( {
        className: 'ce-marker',
        iconUrl: 'images/centro_salud_gps.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    } );


    var iconMarkerCama = L.icon( {
        className: 'cama-marker',
        iconUrl: 'images/cama_gps.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    } )

    var iconMarkerCacu = L.icon( {
        className: 'cacu-marker',
        iconUrl: 'images/cacu_gps.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    } )

    var iconMarkerMovil = L.icon( {
        className: 'movil-marker',
        iconUrl: 'images/unidad_movil.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    } )

    var iconMarkerHospital = L.icon( {
        className: 'hospital-marker',
        iconUrl: 'images/hospital.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    } )

    var iconMarkerApoyo = L.icon( {
        className: 'apoyo-marker',
        iconUrl: 'images/unidad_apoyo.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    } )

    var iconMarkerClient = L.icon( {
        iconUrl: 'images/cliente.png',
        iconSize: [114, 114],
        iconAnchor: [57, 115]
    } )
    // END CREATING ICONS FOR DISPLAY ON MAP

    // let carranza = L.marker( [32.614267, -115.409125], { icon: iconMarker } ).addTo( map );
    // L.marker( [32.222579, -115.169433], { icon: iconMarker, alt: 'texto' } ).addTo( map ).bindTooltip( "test" ).openTooltip().
    //     on( 'click', function ( e ) {
    //         $( '#newModal' ).modal( 'show' );
    //     } )

    //START SCRIPT TO CHANGE POSITION MAP WHEN CHANGE SELECT OPTION
    $( '.selectpicker' ).on( 'change', function () {
        var selected = []
        selected = $( '.selectpicker' ).val()
        console.log( selected ); //Get the multiple values selected in an array
        console.log( selected.length ); //Length of the array
        var array = selected.split( ',' );
        latitudeLastPoint = array[0];
        longitudeLastPoint = array[1];
        map.setView( new L.LatLng( array[0], array[1] ), 16 );
    } );

    //END SCRIPT TO CHANGE POSITION MAP WHEN CHANGE SELECT OPTION

    //Option for map
    map.doubleClickZoom.enable()

    //Option for map to get lat and lan
    // myMap.on('dblclick', e => {
    //   let latLng = myMap.mouseEventToLatLng(e.originalEvent);
    // console.log(latLng.lat +'-'+ latLng.lng)
    //   L.marker([latLng.lat, latLng.lng], { icon: iconMarker }).addTo(myMap)
    // })

    // GET CURRENTLY POSITION OF USER


    navigator.geolocation.getCurrentPosition(
        ( pos ) => {
            const { coords } = pos;
            const { latitude, longitude } = coords;
            map.setView( [latitude, longitude], 10 );
            L.marker( [latitude, longitude], { icon: iconMarkerClient } ).addTo( map );
            // console.log( coords );
            setTimeout( () => {
                map.panTo( new L.LatLng( latitude, longitude ) )
            }, 5000 )
        },
        ( error ) => {
            console.log( error )
        },
        {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        } );

    // START FILL MAP IS EXIST IN LOCAL STORAGE
    loadMapFromLocalStorage();
    // END FILL MAP IS EXIST IN LOCAL STORAGE

    //START SECTION TO CONVERT EXCEL FILE IN JSON DATA
    let selectedFile;
    // console.log( window.XLSX );
    document.getElementById( 'input' ).addEventListener( "change", ( event ) => {
        selectedFile = event.target.files[0];
    } )

    document.getElementById( 'convertData' ).addEventListener( "click", () => {

        if ( selectedFile ) {
            let fileReader = new FileReader();
            fileReader.readAsBinaryString( selectedFile );
            fileReader.onload = ( event ) => {
                let data = event.target.result;
                let workbook = XLSX.read( data, { type: "binary" } );
                workbook.SheetNames.forEach( sheet => {
                    let rowObject = XLSX.utils.sheet_to_row_object_array( workbook.Sheets[sheet] );
                    jsonData = JSON.stringify( rowObject, undefined, 4 );
                    //display json in html
                    // document.getElementById( "jsondata" ).innerHTML = jsonData; 
                    //createPointsOnMap( rowObject );
                    localStorage.setItem( 'datos', JSON.stringify( rowObject ) );
                    iteration = 0;
                    interval = setInterval( function () {
                        createPointsOnMap( rowObject );
                    }, 100 ) //call it every 500 milliseconds

                    // setTimeout( () => { $( '#modalUpExcel' ).modal( 'hide' ); }, 2000 );

                } );
            }
        }
    } );

    //END SECTION TO CONVERT EXCEL FILE IN JSON DATA

    // START FUNCTION TO CREATE SEVERAL POINTS ON MAP
    var interval;
    var iteration = 0;
    var percent = 0;


    function createPointsOnMap ( dataJson ) {
        let stringCoord = null;
        // console.log('json de funcion:'+ JSON.stringify( dataJson, undefined, 4 ));
        console.log( 'dimension de json:' + dataJson.length )
        let jsonLength = dataJson.length;

        x = dataJson[iteration];
        //Start fit progress bar
        iteration++;
        percent = ( iteration * 100 ) / jsonLength;
        percent = Math.round( percent );
        console.log( 'porcentaje:' + percent );
        updateProgressBar();
        document.getElementById( "jsondata" ).innerHTML = percent + '%';
        //End to fit progress bar

        //Check Jurisdiccion and change class of mark


        if ( dataJson[key].TIPO_UNIDAD == "CE" ) {
            L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarker } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
        }
        if ( dataJson[key].TIPO_UNIDAD == "HOSPITAL" ) {
            L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerHospital } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
        }
        if ( dataJson[key].TIPO_UNIDAD == "MOVIL" ) {
            L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerMovil } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
        }
        if ( dataJson[key].TIPO_UNIDAD == "APOYO" ) {
            L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerApoyo } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
        }
        //fit select
        stringCoord = dataJson[key].LATITUD + ',' + dataJson[key].LONGITUD;
        const $select = $( '#selectpicker' );
        $select.append( $( "<option>", {
            value: stringCoord,
            text: `${dataJson[key].NOMBRE_DE_LA_UNIDAD}` + ' (' + `${dataJson[key].NOMBRE_DEL_MUNICIPIO}` + ')'
        } ) );


        if ( iteration >= jsonLength ) {
            // to reset the selector and get all options
            $( "#selectpicker" ).selectpicker( "refresh" );
            clearTimeout( interval );
        }
    }

    // CREATE POINTS USING FOR
    function createMultiplesPointsOnMap ( dataJson ) {

        let stringCoord = null;

        for ( var key in dataJson ) {

            //fill array of object for count units
            let resultado = countUnits.find( units => units.jurisdiccion === dataJson[key].JURISDICCION );
            if ( typeof resultado == "undefined" ) {
                countUnits.push( {
                    jurisdiccion: dataJson[key].JURISDICCION,
                    ce: dataJson.filter( item => item.TIPO_UNIDAD === 'CE' && item.JURISDICCION === dataJson[key].JURISDICCION ).length,
                    movil: dataJson.filter( item => item.TIPO_UNIDAD === 'MOVIL' && item.JURISDICCION === dataJson[key].JURISDICCION ).length,
                    hospital: dataJson.filter( item => item.TIPO_UNIDAD === 'HOSPITAL' && item.JURISDICCION === dataJson[key].JURISDICCION ).length,
                    apoyo: dataJson.filter( item => item.TIPO_UNIDAD === 'APOYO' && item.JURISDICCION === dataJson[key].JURISDICCION ).length
                } );
            }

            //Change Class for Jurisdiccion
            iconMarker.options.className = "ce-marker j" + dataJson[key].CLAVE_DE_LA_JURISDICCION;
            iconMarkerMovil.options.className = "movil-marker j" + dataJson[key].CLAVE_DE_LA_JURISDICCION;
            iconMarkerHospital.options.className = "hospital-marker j" + dataJson[key].CLAVE_DE_LA_JURISDICCION;
            iconMarkerApoyo.options.className = "apoyo-marker j" + dataJson[key].CLAVE_DE_LA_JURISDICCION;
            iconMarkerCama.options.className = "cama-marker j" + dataJson[key].CLAVE_DE_LA_JURISDICCION;
            iconMarkerCacu.options.className = "cacu-marker j" + dataJson[key].CLAVE_DE_LA_JURISDICCION;

            //Select icon
            if ( dataJson[key].TIPO_UNIDAD == "CE" ) {
                if ( dataJson[key].TIPO_CANCER == "CM" ) {
                    L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerCama } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
                } else if ( dataJson[key].TIPO_CANCER == "CCU" ) {
                    L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerCacu } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
                }
                else {
                    L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarker } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
                }
            } else
                if ( dataJson[key].TIPO_UNIDAD == "HOSPITAL" ) {
                    L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerHospital } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
                } else
                    if ( dataJson[key].TIPO_UNIDAD == "MOVIL" ) {
                        L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerMovil } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
                    } else
                        if ( dataJson[key].TIPO_UNIDAD == "APOYO" ) {
                            L.marker( [`${dataJson[key].LATITUD}`, `${dataJson[key].LONGITUD}`], { icon: iconMarkerApoyo } ).addTo( map ).bindTooltip( dataJson[key].NOMBRE_DE_LA_UNIDAD ).openTooltip().bindPopup( `<p>Teléfono<br/> ${dataJson[key].LADA} ${dataJson[key].TELEFONO}.</p><p>Google Maps <br><a href="http://maps.google.com/maps?q=${dataJson[key].LATITUD},${dataJson[key].LONGITUD}" target="_blank">Clíck Aquí</a></p>` ).openPopup();
                        }
            // Fit select
            stringCoord = dataJson[key].LATITUD + ',' + dataJson[key].LONGITUD;
            const $select = $( '#selectpicker' );
            $select.append( $( "<option>", {
                value: stringCoord,
                text: `${dataJson[key].NOMBRE_DE_LA_UNIDAD}` + ' (' + `${dataJson[key].NOMBRE_DEL_MUNICIPIO}` + ')'
            } ) );

            //Change color
            $( ".center-health, .jurisdiction" ).css( "background-color", "white" );
            $( ".center-health, .jurisdiction" ).css( "color", "#7386D5" );
            $( ".center-health, .jurisdiction" ).addClass( "selected" );

            // to reset the selector and get all options
            $( "#selectpicker" ).selectpicker( "refresh" );

        }

        console.log( countUnits );
    }
    // END FUNCTION TO CREATE SEVERAL POINTS ON MAP

    // START FUNCTION TO FILL BAR
    function updateProgressBar () {
        $( '.progress-bar' ).css( 'width', percent + '%' );
        $( '.progress-bar' ).attr( 'aria-valuenow', percent );
        $( '.progress-bar' ).text( percent + '%' );
    }
    // END FUNCTION TO FILL BAR


    //START FUNCTION TO CHECK IF MAP IS ON LOCALSTORAGE
    function loadMapFromLocalStorage () {
        if ( localStorage.getItem( "datos" ) === null ) {
            console.log( 'No exist Map in LocalStorage' );
        } else {
            console.log( 'Exist Map on LocalStorage' )
            //load map from local storage
            var loadMap = localStorage.getItem( 'datos' );
            //convert to json
            loadMap = JSON.parse( loadMap );
            createMultiplesPointsOnMap( loadMap )
            // createPointsOnMap(loadMap);
        }
        setValuesUnits();
    }

    // START SIDEBAR CODE INTERACTION
    $( "#sidebar-right" ).mCustomScrollbar( {
        theme: "minimal"
    } );

    $( '#dismiss, .overlay-sidebar-right' ).on( 'click', function () {
        $( '#sidebar-right' ).removeClass( 'active' );
        $( '.overlay-sidebar-right' ).removeClass( 'active' );
    } );

    $( '#sidebarCollapse' ).on( 'click', function () {
        $( '#sidebar-right' ).addClass( 'active' );
        $( '.overlay-sidebar-right' ).addClass( 'active' );
        $( '.collapse.in' ).toggleClass( 'in' );
        $( 'a[aria-expanded=true]' ).attr( 'aria-expanded', 'false' );
    } );

    $( '.center-health, .jurisdiction' ).click( function () {
        // map._panes.markerPane.remove();
        id = this.id;
        // if(id == "ce"){
        //     console.log("entro a ce");
        //     $(".ce-marker").addClass("d-none");
        // }
        className = $( "#" + id ).attr( 'class' );
        if ( ~className.indexOf( "selected" ) ) {
            $( "#" + id ).css( "background-color", "#6d7fcc" );
            $( "#" + id ).css( "color", "#fff" );
            $( "#" + id ).removeClass( "selected" );
            $( "." + id ).addClass( "d-none" );


        } else {
            $( "#" + id ).css( "background-color", "white" );
            $( "#" + id ).css( "color", "#7386D5" );
            $( "#" + id ).addClass( "selected" );
            $( "." + id ).removeClass( "d-none" );
        }
    } );

    // START FUNCTION TO FRONTEND COUNT UNITS
    function setValuesUnits(){
        for (const key in countUnits) {
            $( "#countMenu" ).append( `
            <dt id="contj1name" class="col-sm-12">${countUnits[key].jurisdiccion}</dt>
            <dd id="contj1ce" class="col-sm-12">Centros de Salud: ${countUnits[key].ce}</dd>
            <dd id="contj1movil" class="col-sm-12">Unidades Móviles: ${countUnits[key].movil}</dd>
            <dd id="contj1hospital" class="col-sm-12">Hospitales: ${countUnits[key].hospital}</dd>
            <dd id="contj1hospital" class="col-sm-12">Apoyo: ${countUnits[key].apoyo}</dd>
            `);
        }
    }

} );